require_relative '../modules/decorator' # подключаю сюда модуль Decorator, который просто улучшает отображение результатов

class Report
  include Decorator # Вообще, модули это мешочки с методами. Команда include(есть еще exclude, кстати) как бы рассыпает этот мешок где тебе нужно.
  # В данном случае мешочек с методами мы рассыпали в классе, значит все, что есть Decorator доступно в Report.

  def initialize(all_vms, prices) # Нашему классу Report нужно дать все объекты VirtualMachine и весь прайс-лист. Тогда он сможет все посчитать.
    @all_vms = all_vms
    @prices = prices
  end

  def most_expensive_vms(number)
    # В самом низу класса Report есть метод sort_by_cost. Он подсчитывает полную стоимость всех виртуальных машин, сортирует их по цене и возвращает массив.
    # Мы берем последние number-количество элементов из этого массива(как самые дорогие виртуальные машины), для удобства их реверсим и записываем результат в переменную sorted_vms.
    sorted_vms = sort_by_cost.last(number).reverse

    beautiful_output(sorted_vms, 'Самые дорогие виртуальные машины:', 1) # Это тот самый метод из модуля Decorator, который мы получили использовав команду include Decorator.
  end

  def cheapest_vms(number)
    # работа этого метода очень схоже с работой метода, что повыше.
    sorted_vms = sort_by_cost.first(number)

    beautiful_output(sorted_vms, 'Самые дешевые виртуальные машины:', 1)
  end

  def most_capacious_vms(number, type) # В этом методе нас просят найти самую объемную ВМ по параметру type.
    results = []
    # Следующая конструкция %w(sas sata ssd) равняется массиву ['sas', 'sata', 'ssd'].
    if %w(sas sata ssd).include?(type) # Тут мы смотрим является ли аргумент type жестким диском.
      @all_vms.each do |vm| # Если да, то смотрим какие ВМ содержат нужный нам жесткий диск.
        if vm.hdd_type == type
          results << [vm.hdd_capacity, vm]
        end
      end
    else # Если аргумент type не жесткий диск, значит это будет либо cpu, либо ram.
      @all_vms.each do |vm|
        # Далее проиходит очень интересная, но простая вещь.
        # У на есть аргумент type, который либо содержит в себе CPU либо RAM, но я точно не знаю что именно. Чтобы себя избавить от страданий я использую метод send.
        # Если я его вызову так: vm.send('cpu') это будет равняться vm.cpu. Могу точно также вызывать метод 'ram'.
        volume = vm.send(type.downcase)
        results << [volume, vm]
      end
    end
    results.sort_by! { |vm| vm[0] } # Тут сортируем по объему.
    results = results.last(number).reverse # Как и в первом методе берем последние самые объемные ВМки.

    beautiful_output(results, "По типу #{type} самые объемные ВМ это:", 2)
  end

  def most_additonal_hdds(number, hdd_type = nil) # Здесь идет поиск ВМ с самым большим количеством дополнительных дисков
    results = []
    @all_vms.each do |vm|
      # Команда next тут означает, что если в машине нету вообще дополнительных дисков, то мы сразу перекращаем выполнять этот цикл и начинаем следующий.
      # То есть переходим на следующую ВМ.
      next if vm.additional_hdds.empty?
      count_hdds = 0

      if hdd_type # Если нам в аргументах передали hdd_type, то мы начинаем строго следить за типом допольнительных дисков
        vm.additional_hdds.each do |hdd|
          count_hdds += 1 if hdd.type == hdd_type # Если тип дополнительного диска будет равен заданному hdd_type то мы сразу фиксируем этом - count_hdds вырастает на 1.
        end
      else
        count_hdds = vm.additional_hdds.count # Если в аргументах не давали hdd_type,
        # то можно просто сразу подсчитать все дополнительные диски у ВМ, не переживая какого они типа
      end

      results << [count_hdds, vm] # Добавляем массив с количеством дополнительных hdd и объектом класса VirtualMachine в общий массив с результатами.
    end
    results.sort_by! { |vm| vm[0] } # Сортируем по количеству дополнительных дисков
    results = results.last(number).reverse
    # Далее красиво выводим.
    if hdd_type
      beautiful_output(results, "Виртуальные машины с самым большим количеством дополнительных дисков типа #{hdd_type}:", 3)
    else
      beautiful_output(results, 'Виртуальные машины с самым большим количеством дополнительных дисков:', 3)
    end
  end

  def biggest_extra_space(number, hdd_type = nil) # Здесь выводим ВМ с самым объемным дополнительным местом.
    results = []
    @all_vms.each do |vm|
      next if vm.additional_hdds.empty? # Что тут происходит я описывал в методе выше.

      total_capacity = 0 # здесь будет храниться общий объем одной ВМ

      vm.additional_hdds.each do |hdd|
        if hdd_type # Если нас интересует какой-то конкретный тип дополнительных hdd, то мы начинаем их отбирать.
          total_capacity += hdd.capacity if hdd.type == hdd_type
        else # Если нет, то все записываем в total_capacity
          total_capacity += hdd.capacity
        end
      end

      results << [total_capacity, vm] # Как обычно записываем массив с данными в общий массив в результатми
    end
    results.sort_by! { |vm| vm[0] } # Сортируем по объему.
    results = results.last(number).reverse
    # И красиво выводим результат.
    if hdd_type
      beautiful_output(results, "Виртуальные машины, где больше всего дополнительной памяти типа #{hdd_type}:", 4)
    else
      beautiful_output(results, 'Виртуальные машины, где больше всего дополнительной памяти:', 4)
    end
  end

  private

  def sort_by_cost # В этом методе мы подсчитываем стоимость каждой виртуальной машины.
    results = []
    @all_vms.each do |vm| # Берем каждую ВМ и начинает выполнять подсчет
      full_cost = 0

      full_cost += vm.cpu * @prices['cpu'].to_i
      full_cost += vm.ram * @prices['ram'].to_i
      full_cost += vm.hdd_capacity * @prices[vm.hdd_type].to_i

      unless vm.additional_hdds.empty? # Эта конструкция означает дословно следущее: "Если массив с дополнительными жесткими дисками у ВМ не пустой, то выполняем подсчет их стоимости".
        vm.additional_hdds.each do |hdd|
          full_cost += hdd.capacity * @prices[hdd.type].to_i
        end
      end

      results << [full_cost, vm] # Массив с ценой и объектом виртуальной машины мы загоняем в один большой массив с результатами.
    end
    results.sort_by! { |vm| vm[0] } # Здесь идет сортировка по цене.
  end
end
