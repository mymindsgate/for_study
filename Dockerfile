FROM ruby:2.7

COPY . /app

WORKDIR /app

ENV n 11

CMD ["ruby", "app.rb"]
