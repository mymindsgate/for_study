module Decorator # Декоратор не самая важная вещь в нашей задаче, разибрать его нету особого смысла. Он просто украшает ответы на вопросы, чтобы их было приятнее читать.
  def beautiful_output(vms, topic, type)
    case type
    when 1
      puts '========================'
      puts topic
      vms.each do |vm_with_price|
        price = vm_with_price[0]
        vm = vm_with_price[1]
        puts "#{price/100.0} руб.. Идентификатор ВМ: #{vm.id}, где CPU: #{vm.cpu}, RAM: #{vm.ram}, HDD: #{vm.hdd_type} с размером #{vm.hdd_capacity} ГБ и с #{vm.additional_hdds.count} дополнительными дисками."
      end
      puts '========================'

    when 2
      puts '========================'
      puts topic
      vms.each do |result|
        res = result[0]
        vm = result[1]
        puts "#{res}. Идентификатор ВМ: #{vm.id}"
      end
      puts '========================'

    when 3
      puts '========================'
      puts topic
      vms.each do |result|
        count_hdds = result[0]
        vm = result[1]
        puts "#{count_hdds} шт.. Идентификатор ВМ: #{vm.id}, где CPU: #{vm.cpu}, RAM: #{vm.ram} и HDD: #{vm.hdd_type} размером #{vm.hdd_capacity} ГБ."
      end
      puts '========================'

    when 4
      puts '========================'
      puts topic
      vms.each do |result|
        total_capacity = result[0]
        vm = result[1]
        puts "#{total_capacity} ГБ. Идентификатор ВМ: #{vm.id}, где CPU: #{vm.cpu}, RAM: #{vm.ram} и HDD: #{vm.hdd_type} размером #{vm.hdd_capacity} ГБ."
      end
      puts '========================'
    end

  end
end
